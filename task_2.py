#!/usr/bin/python
import re

def second_longest_character_chain(input_text):
    if isinstance(input_text, str) == False:
        return None

    if len(input_text) >= 1000000:
        return None

    match = re.match(r'^[a-z]*$',input_text, 0)
    if match is None:
        return None
            
    current = 1
    longest = 0
    second = 0
    input_text_lenght = len(input_text)

    for i in range (input_text_lenght):
        if ((i < input_text_lenght - 1) and (input_text[i] == input_text[i+1])):
            current += 1
        else:
            if current > longest: 
                second = longest 
                longest = current
    
            elif second < current and longest != current:
                second = current
            current = 1
    return second

