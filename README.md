# Overview  
## Task 1.  
Check if the number is a prime number. The function should return True for all prime numbers and False for all the rest. Function should handle numbers from Integer range.

## Task 2.  
Find the length of the second longest chain of the same character in provided text. Function should return the length as a digit. Input is a string (a-z) where  0 ≤ len(input) < 10^6.

## Usage  
To execute *all test suites*:
```
python -m unittest discover
```

To execute a *standalone test suite*:
```
python task_1_unit_tests.py
```
```
python task_2_unit_tests.py
```
## Test data files  
You can find two additional test data files, one to test the first 10.000 prime **primes.txt** for Task 1. the other one, **invalid_lenght.txt**, to test the limitation of the maximum lenght of string as an input for Task 2.
