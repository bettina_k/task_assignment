import unittest
import task_1 as t

class Task_1_UnitCase(unittest.TestCase):

    def test_invalid_input(self):
        self.assertEqual(t.is_prime("asd"), None)
        self.assertEqual(t.is_prime(""), None)
        self.assertEqual(t.is_prime("123"), None)
        self.assertEqual(t.is_prime("asd"), None)
        self.assertEqual(t.is_prime(-1), None)
        self.assertEqual(t.is_prime(0), None)
        self.assertEqual(t.is_prime(1), None)
        
    def test_valid_input_prime(self):
        f = open("primes.txt", "r")
        lines = f.readlines()
        for line in lines:
            numbers = line.split(",")
            for number in numbers:
                intNumber = int(number)
                self.assertTrue(t.is_prime(intNumber))
        f.close()
    
    def test_valid_input_not_prime(self):   
        self.assertFalse(t.is_prime(6), False)
        self.assertFalse(t.is_prime(99), False)
        self.assertFalse(t.is_prime(165468), False)   

if __name__ == "__main__":
    unittest.main()    
