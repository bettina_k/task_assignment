#!/usr/bin/python

def is_prime(number):
    if isinstance(number, int) == False or number <= 1:
        return None

    if number <= 3:
        return number > 1
    elif number % 2 == 0 or number % 3 == 0:
        return False
    divisor = 5
    while divisor * divisor <= number:   
        if number % divisor == 0 or number % (divisor + 2) == 0:
            return False
        divisor = divisor + 6
    return True