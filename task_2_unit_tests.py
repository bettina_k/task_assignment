import unittest
import task_2 as t

class Task_2_UnitCase(unittest.TestCase):

    def test_invalid_input_type(self):
        self.assertEqual(t.second_longest_character_chain(1), None)
        self.assertEqual(t.second_longest_character_chain(1.1), None)
    
    def test_invalid_charachter_input(self):
        self.assertEqual(t.second_longest_character_chain("1"), None)
        self.assertEqual(t.second_longest_character_chain("a1"), None)
        self.assertEqual(t.second_longest_character_chain("."), None)
        self.assertEqual(t.second_longest_character_chain("a."), None)
        self.assertEqual(t.second_longest_character_chain("ZX"), None)
        self.assertEqual(t.second_longest_character_chain("aY"), None)
        self.assertEqual(t.second_longest_character_chain("Y1"), None)
        self.assertEqual(t.second_longest_character_chain("Y."), None)
        self.assertEqual(t.second_longest_character_chain("aa bbb"), None)

    def test_invalid_input_length(self):
        f = open("invalid_length.txt", "r")
        content = f.read()
        f.close()
        self.assertEqual(t.second_longest_character_chain(content), None)

    def test_valid_input(self):
        self.assertEqual(t.second_longest_character_chain("aaab"), 1)
        self.assertEqual(t.second_longest_character_chain("aaabaa"), 2)
        self.assertEqual(t.second_longest_character_chain("aaabbaaaa"), 3)
        self.assertEqual(t.second_longest_character_chain("aabccccdddd"), 2)
        self.assertEqual(t.second_longest_character_chain("aabbbcwwww"), 3)
        self.assertEqual(t.second_longest_character_chain("cccccaeeeeebbaaaaee"), 4)
        self.assertEqual(t.second_longest_character_chain("aaabbbccccdddd"), 3)
        self.assertEqual(t.second_longest_character_chain("aaabbb"), 0)
        self.assertEqual(t.second_longest_character_chain("aaabbbccdd"), 2)  
        self.assertEqual(t.second_longest_character_chain("aaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbccccccddddddddddaaaaaaaaaaaaaaaaa"), 18)
            
if __name__ == "__main__":
    unittest.main()